export const isMobile = (): boolean => (
  /Android|webOS|iPhone|iPad|iPod/i.test(navigator.userAgent)
);

export function timeout(ms: number): Promise<void> {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
