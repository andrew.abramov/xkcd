export type IndexedComic = {
  num: number;
  date?: number;
  title?: string;
  image?: string;
  width: number;
  height: number;
};

export type IndexedComics = {
  [key: string]: IndexedComic;
};

export type ComicStrip = {
  num: number;
  day: string;
  month: string;
  year: string;
  title: string;
  safe_title?: string;
  img: string;
  news?: string;
  transcript?: string;
  alt?: string;
  link?: string;
};
