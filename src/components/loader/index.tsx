import React from 'react';
import loading from '../../images/loading.gif';

import styles from './loader.scss';

const Loader = () => (
  <div className={styles.root}>
    <img
      className={styles.image}
      src={loading}
      alt="Loading..."
    />
  </div>
);

export default Loader;
