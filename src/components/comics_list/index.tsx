import React, { ReactNode } from 'react';

import _debounce from 'lodash/debounce';
import { isMobile } from '../../utils';
import { IndexedComics } from '../../types/comic_strip';

import styles from './comics_list.scss';

const TARGET_HEIGHT = 200;

type Props = {
  data: IndexedComics;
  onOpenStrip: (id: number) => void;
};

type State = {
  rows?: {
    height: number;
    items: string[];
  }[];
  displayRows: {
    [key: string]: boolean;
  };
  viewportWidth: number;
};

class ComicsList extends React.Component<Props, State> {
  state: State = {
    displayRows: {},
    viewportWidth: window.innerWidth,
  };

  observer?: IntersectionObserver;

  handleResize = _debounce(this.calculateGrid, 200).bind(this);

  constructor(props: Props) {
    super(props);

    this.handleIntersection = this.handleIntersection.bind(this);
    this.initObserver = this.initObserver.bind(this);
  }

  componentDidMount(): void {
    this.calculateGrid();
    if (isMobile()) {
      window.addEventListener('orientationchange', this.handleResize);
    } else {
      window.addEventListener('resize', this.handleResize);
    }
  }

  componentWillUnmount(): void {
    this.observer?.disconnect();
    if (isMobile()) {
      window.removeEventListener('orientationchange', this.handleResize);
    } else {
      window.removeEventListener('resize', this.handleResize);
    }
  }

  handleIntersection(entries: IntersectionObserverEntry[]): void {
    const { displayRows } = this.state;
    const updatedDisplayRows = { ...displayRows };

    // Rows which we want to show
    entries
      .filter((entry) => entry.isIntersecting)
      .map((entry) => {
        const target = entry.target as HTMLDivElement;
        return target.dataset?.row;
      }).forEach((rowIndex) => {
        if (rowIndex) {
          updatedDisplayRows[rowIndex] = true;
        }
      });

    // Rows which we want to hide
    entries
      .filter((entry) => !entry.isIntersecting)
      .map((entry) => {
        const target = entry.target as HTMLDivElement;
        return target.dataset?.row;
      }).forEach((rowIndex) => {
        if (rowIndex) {
          updatedDisplayRows[rowIndex] = false;
        }
      });

    this.setState({
      displayRows: updatedDisplayRows,
    });
  }

  initObserver(): void {
    this.observer?.disconnect();

    if (window.IntersectionObserver) {
      const viewportHeight = window.innerHeight;
      this.observer = new IntersectionObserver(this.handleIntersection, {
        rootMargin: `${viewportHeight * 2}px 0px ${viewportHeight * 3}px 0px`,
      });
    }

    const rowElements = document.querySelectorAll(`.${styles.row}`);
    rowElements.forEach((row) => this.observer?.observe(row));
  }

  calculateGrid(): void {
    console.time('calculate grid');
    const { data } = this.props;
    const viewportWidth = window.innerWidth - 40;
    const rows = [];
    let rowItems: string[] = [];
    let currentRowWidth = 0;
    const entries = Object.keys(data);
    // display oldest first
    entries.reverse();
    entries.forEach((id) => {
      const item = data[id];
      rowItems.push(item.num.toString());
      const { width, height } = item;
      currentRowWidth += Math.round((TARGET_HEIGHT / height) * width) + 3;
      if (currentRowWidth >= viewportWidth) {
        const prop = currentRowWidth / viewportWidth;
        rows.push({
          height: Math.floor(TARGET_HEIGHT / prop),
          items: [...rowItems],
        });
        rowItems = [];
        currentRowWidth = 0;
      }
    });

    if (rowItems.length) {
      rows.push({
        items: rowItems,
        height: TARGET_HEIGHT,
      });
    }
    console.timeEnd('calculate grid');

    this.setState({
      rows,
      viewportWidth,
    }, this.initObserver);
  }

  render(): ReactNode {
    const { data } = this.props;
    const {
      rows,
      displayRows,
      viewportWidth,
    } = this.state;

    if (!rows) {
      return null;
    }

    return (
      <div className={styles.wrapper}>
        <div style={{
          width: `${viewportWidth}px`,
        }}
        >
          {rows.map((row, index) => (
            <div
              data-row={index}
              className={index === rows.length - 1 ? styles.row_last : styles.row}
              key={`${row.items[0]}_${row.height}`}
              style={{
                height: `${row.height}px`,
                transform: 'translateZ(0)',
              }}
            >
              {displayRows[index] ? row.items.map((id) => (
                <div
                  tabIndex={0}
                  role="button"
                  onClick={() => this.props.onOpenStrip(data[id].num)}
                  key={data[id].num}
                  className={styles.row_item}
                  style={{
                    width: `${row.height * (data[id].width / data[id].height)}px`,
                    height: `${row.height}px`,
                  }}
                >
                  <picture>
                    <source srcSet={`https://xkcd.io/thumbs/${data[id].num}.webp`} type="image/webp" />
                    <source srcSet={`https://xkcd.io/thumbs/${data[id].num}.jpg`} type="image/jpeg" />
                    <img
                      src={`https://xkcd.io/thumbs/${data[id].num}.jpg`}
                      alt={data[id].title}
                    />
                  </picture>
                </div>
              )) : null}
            </div>
          ))}
        </div>
      </div>
    );
  }
}

export default ComicsList;
