import React from 'react';
import { shallow } from 'enzyme';

import ComicsList from './index';
import { mockData } from '../../../test/mockData';

describe('<ComicsList />', () => {
  it('snapshot: empty data', () => {
    const wrapper = shallow<ComicsList>(
      <ComicsList data={{}} onOpenStrip={jest.fn()} />,
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('snapshot: mock data with 404', () => {
    const wrapper = shallow<ComicsList>(
      <ComicsList data={mockData} onOpenStrip={jest.fn()} />,
    );

    expect(wrapper).toMatchSnapshot();
  });
});
