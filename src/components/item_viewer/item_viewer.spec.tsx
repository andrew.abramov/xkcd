import React from 'react';
import { shallow } from 'enzyme';

import ItemViewer from '.';
import { mockData } from '../../../test/mockData';

describe('<ItemViewer />', () => {
  it('snapshot: default', () => {
    const wrapper = shallow<ItemViewer>(
      <ItemViewer
        id={1}
        data={mockData}
        onShowStrip={jest.fn()}
        onCloseViewer={jest.fn()}
      />,
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('snapshot: should not fail without image', () => {
    const wrapper = shallow<ItemViewer>(
      <ItemViewer
        id={404}
        data={mockData}
        onShowStrip={jest.fn()}
        onCloseViewer={jest.fn()}
      />,
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('callback: click close should trigger onShowStrip', () => {
    const onCloseViewer = jest.fn();

    const wrapper = shallow<ItemViewer>(
      <ItemViewer
        id={404}
        data={mockData}
        onShowStrip={jest.fn()}
        onCloseViewer={onCloseViewer}
      />,
    );

    wrapper.find('button').simulate('click');
    expect(onCloseViewer).toHaveBeenCalledTimes(1);
  });

  it('callback: clicking on overlay should close the viwere', () => {
    const onCloseViewer = jest.fn();

    const wrapper = shallow<ItemViewer>(
      <ItemViewer
        id={404}
        data={mockData}
        onShowStrip={jest.fn()}
        onCloseViewer={onCloseViewer}
      />,
    );

    wrapper.find({ 'data-overlay': 'true' }).simulate('click');
    expect(onCloseViewer).toHaveBeenCalledTimes(1);
  });
});
