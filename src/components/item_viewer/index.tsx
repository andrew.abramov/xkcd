import React, { ReactNode } from 'react';
import { format } from 'date-fns';
import { ComicStrip, IndexedComics } from '../../types/comic_strip';

import styles from './item_viewer.scss';
import getComicById from '../../api';
import Loader from '../loader';

type Props = {
  id?: number;
  data: IndexedComics;
  onShowStrip: (id: number) => void;
  onCloseViewer: () => void;
};

type State = {
  detailedData: {
    [key: number]: Partial<ComicStrip> & {
      loading?: boolean;
    };
  };
  loadedImages: {
    [key: number]: boolean;
  }
};

class ItemViewer extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.handleKeyDown = this.handleKeyDown.bind(this);
  }

  state: State = {
    detailedData: {},
    loadedImages: {},
  };

  componentDidMount(): void {
    document.addEventListener('keydown', this.handleKeyDown);
  }

  componentDidUpdate(prevProps: Readonly<Props>): void {
    const { id } = this.props;
    const { detailedData } = this.state;
    if (id && id !== prevProps.id && !detailedData[id]) {
      this.loadDetails(id);
    }
  }

  componentWillUnmount(): void {
    document.removeEventListener('keydown', this.handleKeyDown);
  }

  handleKeyDown(e: KeyboardEvent): void {
    if (e.key === 'Escape') {
      this.props.onCloseViewer();
    }

    const { id } = this.props;

    // TODO: use the indexes instead of ids
    if (e.key === 'ArrowLeft' && typeof id === 'number') {
      this.props.onShowStrip(id + 1);
    }

    if (e.key === 'ArrowRight' && typeof id === 'number') {
      this.props.onShowStrip(id - 1);
    }
  }

  handleImageLoaded(id: number): void {
    this.setState((state) => ({
      loadedImages: {
        ...state.loadedImages,
        [id]: true,
      },
    }));
  }

  async loadDetails(id: number): Promise<void> {
    this.setState((state) => ({
      detailedData: {
        ...state.detailedData,
        [id]: {
          loading: true,
        },
      },
    }));
    const item = await getComicById(id);
    this.setState((state) => ({
      detailedData: {
        ...state.detailedData,
        [id]: item,
      },
    }));
  }

  render(): ReactNode {
    const { id, data } = this.props;
    const { detailedData, loadedImages } = this.state;

    if (!id) {
      return null;
    }

    const item = data[id];
    const itemDetails = detailedData[id];

    return (
      <div
        className={styles.overlay}
      >
        <div className={styles.overlay_click} onClick={this.props.onCloseViewer} data-overlay="true" />
        <div className={styles.modal}>
          <div className={styles.title_group}>
            <h1 className={styles.title}>{item?.title || 'No name'}</h1>
            {item?.date && <p className={styles.date}>{format(item.date, 'dd MMM yyyy')}</p>}
          </div>
          <div
            key={id}
            className={styles.image_wrap}
            style={{
              maxWidth: `${item?.width}px`,
            }}
          >
            {item?.image ? (
              <>
                {!loadedImages[id] && <Loader />}
                <img
                  onLoad={() => this.handleImageLoaded(id)}
                  className={!loadedImages[id] ? styles.image : styles.image_loaded}
                  src={item.image}
                  alt={item?.title || ''}
                />
              </>
            ) : <Loader />}

            <div style={{
              paddingBottom: `${100 * (item.height / item.width)}%`,
            }}
            />
          </div>
          {(itemDetails && !itemDetails.loading && itemDetails.alt) ? (
            <div className={styles.alt}>
              {itemDetails.alt}
            </div>
          ) : null}

          <button
            title="Close"
            className={styles.close_button}
            type="button"
            onClick={this.props.onCloseViewer}
          >
            Close
          </button>
        </div>
      </div>
    );
  }
}

export default ItemViewer;
