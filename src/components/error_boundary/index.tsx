import React, { ReactNode } from 'react';

type State = {
  error?: Error;
};

const reloadPage = (): void => window.location.reload();

class ErrorBoundary extends React.Component<unknown, State> {
  state: State = {};

  static getDerivedStateFromError(error: Error): State {
    return { error };
  }

  render(): ReactNode {
    const { error } = this.state;
    if (error) {
      return (
        <div>
          <h1>Something went wrong.</h1>
          <p>
            {error?.message || 'Unknown Error'}
          </p>
          <button type="button" onClick={reloadPage}>
            Reload page
          </button>
        </div>
      );
    }

    return this.props.children;
  }
}

export default ErrorBoundary;
