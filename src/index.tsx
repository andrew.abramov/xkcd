import React, { ReactNode } from 'react';
import { render } from 'react-dom';

import { IndexedComic } from './types/comic_strip';
import { timeout } from './utils';

import ErrorBoundary from './components/error_boundary';
import ComicsList from './components/comics_list';
import ItemViewer from './components/item_viewer';
import Loader from './components/loader';

require('reset-css/reset.css');
require('./common.scss');

type State = {
  data?: {
    [key: number]: IndexedComic,
  };
  showId?: number;
};

type Props = unknown;

class App extends React.Component<Props, State> {
  state: State = {};

  constructor(props: Props) {
    super(props);

    this.handleOpenStrip = this.handleOpenStrip.bind(this);
  }

  componentDidMount() {
    this.fetchData();
  }

  async fetchData() {
    // TODO: Handle the error from request
    const response = await fetch('https://xkcd.io/comics.json');
    const indexData = await response.json() as Array<Array<unknown>>;

    console.time('index data');
    const data: State['data'] = {};
    for (let i = 0; i < indexData.length; i += 1) {
      const item = indexData[i];
      const [num, date, title, image, width, height] = item;
      const comic = {
        num,
        date,
        title,
        image,
        width,
        height,
      };

      data[num as number] = comic as IndexedComic;
    }
    console.timeEnd('index data');

    // Make UI loading a little bit longer to be noticeable
    await timeout(700);
    this.setState({
      data,
    });
  }

  handleOpenStrip(showId: number) {
    const { data } = this.state;
    if (showId && data?.[showId]) {
      this.setState({
        showId,
      });
    }
  }

  render(): ReactNode {
    const { data, showId } = this.state;
    return (!data ? (
      <Loader />
    ) : (
      <div>
        {showId && <style>{'body { overflow: hidden; }'}</style>}
        <ItemViewer
          onShowStrip={this.handleOpenStrip}
          onCloseViewer={() => this.setState({ showId: undefined })}
          data={data}
          id={showId}
        />
        <ComicsList
          onOpenStrip={this.handleOpenStrip}
          data={data}
        />
      </div>
    )
    );
  }
}

render(
  <ErrorBoundary>
    <App />
  </ErrorBoundary>, document.getElementById('app'),
);
