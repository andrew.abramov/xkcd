import { ComicStrip } from './types/comic_strip';

const API_URL = 'https://kjzw9sh0pl.execute-api.eu-north-1.amazonaws.com/default/xkcd';

export default async function getComicById(id: number): Promise<ComicStrip> {
  const url = `${API_URL}?id=${id}`;

  const response = await fetch(url);
  const data = await response.json();
  return data;
}
