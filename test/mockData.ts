import { IndexedComics } from '../src/types/comic_strip';

// eslint-disable-next-line import/prefer-default-export
export const mockData: IndexedComics = {
  1: {
    num: 1,
    date: 1136073600000,
    title: 'Mock Title',
    image: 'https://imgs.xkcd.com/comics/everyones_an_epidemiologist.png',
    width: 100,
    height: 100,
  },
  404: {
    num: 1,
    width: 100,
    height: 100,
  },
};
