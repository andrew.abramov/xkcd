module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'jest-environment-jsdom-sixteen',
  collectCoverage: false,
  collectCoverageFrom: ['src/**/*.{ts,tsx}', '!**/node_modules/**'],
  setupFilesAfterEnv: [
    '<rootDir>/test/setup.js',
  ],
  moduleNameMapper: {
    '\\.(scss)$': '<rootDir>/test/stylesMock.js',
    '\\.(gif)$': '<rootDir>/test/gifMock.js',
  },
  snapshotSerializers: [
    'enzyme-to-json/serializer',
  ],
};
