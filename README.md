XKCD app
===

A simple web app that display XKCD comic strips, deployed to https://xkcd.io/

### Environment
* Node.js `12`
* npm `6`

### Install dependencies and build web application

Install dependencies and run `build` script using the following commands:

```sh
npm ci
npm run build
```

To start the web app open the `dist` folder and open `index.html` file using your browser

### Development mode

To run the app in development mode use:

```sh
npm run dev
```

This should run dev version at http://localhost:8080/

### Testing

Components are covered by few simple unit tests which are comparing the snapshots and checking callbacks
To run unit tests use the following command:

```sh
npm run test
```

### Technical aspects

- All components are separate independent modules, which also encapsulate styles (via enabled CSS modules)
- Using typescript to transpile react directly into javascript ES2015 (aka ES6) without babel transpiler

### Code Dependencies

- React.js + react-dom `16.13.1` - main framework
- date-fns `2.16.1` - support for date formatting
- reset-css

### Development and Build Dependencies

- `typescript`
- `webpack 4` and `webpack-cli` + relevant loaders (styles / images)
- `webpack-dev-server` to enable development mode with live updates
- `eslint` with the relevant plugins for Airbnb's ESLint config
- `jest 26` and `enzyme` with typescript support as a test framework
