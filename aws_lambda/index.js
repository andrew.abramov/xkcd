const https = require('https');

function getComicstrip(id) {
  let dataString = '';
  return new Promise((resolve, reject) => {
    https.get(`https://xkcd.com/${id}/info.0.json`, function(request) {
      request.on('data', chunk => {
        dataString += chunk;
      });
      request.on('end', () => {
        resolve({ statusCode: request.statusCode, body: dataString });
      });
      request.on('error', (e) => {
        reject({ statusCode: request.statusCode, body: e.message });
      });
    });
  });
}

exports.handler = async (event) => {
  let id;

  if (event.queryStringParameters && event.queryStringParameters.id) {
    id = parseInt(event.queryStringParameters.id);
  }

  if (isNaN(id) || id < 0) {
    return {
      statusCode: 400,
      body: 'Please provide a valid Comic id'
    }
  }

  const { statusCode, body } = await getComicstrip(id);

  return {
    statusCode,
    body,
  };
};
