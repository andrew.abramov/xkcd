const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

const path = require('path');

const devMode = process.env.NODE_ENV !== 'production';

const plugins = [
  new CopyPlugin({
    patterns: [
      { from: 'src/robots.txt', to: 'robots.txt' },
      { from: 'src/favicon.ico', to: 'favicon.ico' },
    ],
  }),
  new HtmlWebpackPlugin({
    template: './src/index.html',
    inject: true,
  }),
];

if (!devMode) {
  plugins.push(
    new MiniCssExtractPlugin({
      filename: 'static/styles.[hash].css',
    }),
  );
}

const styleLoader = devMode ? 'style-loader' : MiniCssExtractPlugin.loader;

module.exports = {
  entry: './src/index.tsx',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        use: [
          {loader: styleLoader},
          {loader: 'css-loader'},
        ],
      },
      {
        test: /\.scss/,
        use: [
          {
            loader: styleLoader,
            options: devMode ? {} : {
              publicPath: '../'
            }
          },
          {
            loader: 'css-loader',
            options: {
              modules: {
                localIdentName: '[name]__[local]',
              },
            },
          },
          {loader: 'sass-loader'},
        ],
      },
      {
        test: /\.(svg|png|jpg|woff|woff2)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 1000,
              name: 'static/[name].[hash].[ext]',
            },
          },
        ],
      },
      {
        test: /\.(gif)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 100000,
            },
          },
        ],
      },
    ],
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx', '.scss', '.css'],
  },
  output: {
    filename: 'static/index.[hash].js',
    path: path.resolve(__dirname, 'dist'),
  },
  plugins: plugins,
};
